.data
	/* Votre programme assembleur ici... */
	i:
        .int 0

.global func_s

func_s:	
	/* Votre programme assembleur ici... */


	for:
		mov i, %edi
		mov $10, %eax
		cmp %edi, %eax
		je end
		add $1, %edi
		mov %edi, i


// operation a= d+e-b
		mov e, %esi
		mov d, %edx
		add %esi, %edx
		mov b, %ebx
		sub %ebx, %edx
		mov %edx, a

//premier if
	if:
		mov b, %ebx
		sub $1000, %ebx
		mov c, %ecx
		add $500, %ecx
		cmp %ebx, %ecx
		jnae else

		mov c, %ecx
		sub $500, %ecx
		mov %ecx, c

	iff:
		mov b, %ebx
		mov c, %ecx
		cmp %ecx, %ebx
		jnae for

		mov b, %ebx
		sub $500, %ebx
		mov %ebx, b
		jmp for

	else:
		mov e, %esi
		mov b, %ebx
		sub %esi, %ebx
		mov %ebx, b
		mov d, %edx
		add $500, %edx
		mov %edx, d
		jmp for


	end:
	
		ret
