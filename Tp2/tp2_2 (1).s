.global func_s

func_s:

# faire (b*d) et placer le resultat dans  a
flds b 
flds d 
fmulp
fstps a

# faire (b*d)- c et placer le resulat dans a
flds a
flds c
fsubp
fstps a

# faire (f-g), placer le resultat dans st[1], placer la 
# valeur de a ((b*d)-c)) dans st[0] et faire a/(f-g)
# et placer le resultat dans a
flds f
flds g
fsubp 
flds a
fdivp
fstps a

# faire (a+e) et place le resultat dans a 
flds e
flds a
faddp 
fstps a

# faire (g-e) et multiplier le resultat par a et placer le resultat de cette operation dans a
flds g
flds e
fsubrp
flds a
fmulp
fstps a 

# faire (a/f) et placer le resultat dans a
flds a
flds f
fdivrp 
fstps a


	ret
