.globl matrix_transpose_asm

matrix_transpose_asm:
    .globl matrix_transpose_asm

matrix_transpose_asm:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        #On place valeurIni, valeurFin et dim dans un registre
        mov 16(%ebp),%edx #dim qui est la dimension de la matrice dans edx
        mov 12(%ebp), %ecx  #Met valeurFin dans ecx
        mov 8(%ebp), %ebx #Met valeurIni dans eax
        
        movl $0,-4(%ebp) #initie la position j=0 
        
        Boucle1:

            #On initie la position i d'une variable � 0
            movl $0,-8(%ebp) #j=0
            cmp -4(%ebp),%edx #Comparaison entre j et dim sous la form flags 
            jl end #Si j>matorder, on "end" le programme
            
        Boucle2:
          
            cmp -8(%ebp),%edx #Comparaison entre i et dim sous la forme flags 
            jl incrementation #Si c>matorder, on incr�mente r, jusqu'� ce qu'on finisse par end le programme
            
            #valeurFin[i + j * dim] = valeurIni[j + i * dim];
            movl %edx,%esi
            mul -4(%ebp),%esi #j*dim
            addl -8(%ebp),%esi #j+i*dim
            
            movl %edx,%edi
            mul -8(%ebp),%edi #i*dim
            addl -4(%ebp),%edi #r+i*dim
            
            movl (%ebx, %edi, 4), %eax #Met valeurIni[r + c * dim] dans eax
            
            movl %eax, (%ecx, %esi, 4)
            
            inc-8(%ebp) #i++
            jmp Boucle2
        
        incrementation:
            inc-4(%ebp) #r++
            jmp Boucle1
        
        end:
            leave          /* restore ebp and esp */
            ret            /* return to the caller */