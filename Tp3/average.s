globl matrix_transpose_asm

matrix_transpose_asm:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        #On place inmatdata, outmatdata et matorder dans un registre
        mov 16(%ebp),%edx #matorder dans edx
        mov 12(%ebp), %ecx  #utmatdata dans ecx
        mov 8(%ebp), %ebx #inmatdata dans eax�
        
        movl $0,-4(%ebp) #r=0 
		
        
        for1:
            movl $0,-8(%ebp) #c=0
			mov $0, -12(%ebp) #elem=0
            #Condition du for1
            cmp -4(%ebp),%edx #Comparaison entre r et matorder sous la forme flags 
            jng end #Si r>matorder, on "end" le programme
            
        for2:
            #Condition du for2
            cmp -8(%ebp),%edx #Comparaison entre c et matorder sous la forme flags <-- matorder-c
            jng moyenne #Si c>matorder, 
            
            #elem += inmatdata[c + r * matorder];
            movl %edx,%edi
            imul -4(%ebp),%edi #r*matorder
            addl -8(%ebp),%edi #c+r*matorder
            
            addl (%ebx, %edi, 4),-12(%ebp)  #Met inmatdata[r + c * matorder] dans elem
            
            add $1,-8(%ebp) #c++
            jmp for2
        
        incrementation:
            add $1,-4(%ebp) #r++
            jmp for1

        moyenne:
            movl -12(%ebp),%eax 
			idiv %edx
			movl %aex, -4(%ebp)
			add $1,-8(%ebp) #c++
            jmp incrementation

        end:
            leave          /* restore ebp and esp */
            ret            /* return to the caller */
