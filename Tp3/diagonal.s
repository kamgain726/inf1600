.globl matrix_transpose_asm

matrix_transpose_asm:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        #On place inmatdata, outmatdata et matorder dans un registre
        mov 16(%ebp),%edx #matorder dans edx
        mov 12(%ebp), %ecx  #utmatdata dans ecx
        mov 8(%ebp), %ebx #inmatdata dans eax
        
        movl $0,-4(%ebp) #r=0 
        
        for1:
            movl $0,-8(%ebp) #c=0
            #Condition du for1
            cmp -4(%ebp),%edx #Comparaison entre r et matorder sous la forme flags 
            jng end #Si r>matorder, on "end" le programme
            
        for2:
            #Condition du for2
            cmp -8(%ebp),%edx #Comparaison entre c et matorder sous la forme flags <-- matorder-c
            jng incrementation #Si c>matorder, on incr�mente r, jusqu'� ce qu'on finisse par exit le programme

			#D�finition du si(if)
			cmp-4(%ebp),-8(%ebp)#Comparaison entre c et r sous la forme flags 
			jnz si #Si r et ne poss�de pas la m�me valeur, on d�fini la valeur du nombre dans la matrice a 0
            
            #outmatdata[c + r * matorder] = inmatdata[c + r * matorder];
            movl %edx,%esi
            imul -4(%ebp),%esi #r*matorder
            addl -8(%ebp),%esi #c+r*matorder
            
            movl %edx,%edi
            imul -4(%ebp),%edi #r*matorder
            addl -8(%ebp),%edi #c+r*matorder
            
            movl (%ebx, %edi, 4), %eax #Met inmatdata[r + c * matorder] dans eax
            
            movl %eax, (%ecx, %esi, 4)
            
            add $1,-8(%ebp) #c++
            jmp for2
        
        incrementation:
            add $1,-4(%ebp) #r++
            jmp for1

        si:
			movl %edx,%esi
            imul -4(%ebp),%esi #r*matorder
            addl -8(%ebp),%esi #c+r*matorder
			movl $0, %aex
			movl %aex, (%ecx, %esi, 4)
			add $1,-8(%ebp) #c++
            jmp for2

        end:
            leave          /* restore ebp and esp */
            ret            /* return to the caller */